+++
title = "Resume"
description = "GCardona's Resume"
date = "2020-10-20"
author = "Gerard Cardona"
+++

# PROFILE
Inquiring and with experience in roles varying from Business management, IT infrastructures and Computer Science (Software Engineering) to Flotilla Skipper. Seeking to make a career change towards something with a possibility of rotation, in contact with the sea and the ability to progress through the MCA professional pathway. Speaks Catalan, Spanish and English fluently, and understands French to a basic level.

# YACHTING QUALIFICATIONS
- RYA Yachtmaster Offshore Sail (Commercially endorsed)
- RYA Cruising Instructor Sail
- RYA Powerboat Level II
- STCW 95 completed Sept. 2015
- ENG1 Medical passed
- RYA Radar Certificate
- RYA Diesel Engine Maintenance
- RYA Security Awareness

# OTHER QUALIFICATIONS
- Degree in Computer Engineering in UPC Barcelona.
- B2B Sales Certificate in ESADE Barcelona.

# EMPLOYMENT HISTORY

## Sea Experience
### Two summers working in MedSailors
Job involves Yacht skippering, guest and yacht maintenance
management. Also involves cooking for guests.
Skills practiced:
- Leadership
- Guest orientation
- Yacht Maintenance

## Land Experience
### Software Engineering
5+ years involved in top companies working as a Software
Engineer building Web applications.
Companies:
- Softonic (Top 50 organic traffic from 2010 to 2014).
- Travelperk (Fastest Saas growing Startup in Europe 2018).
- Flash Express (In charge of all the IT infrastructure as well as the development of the company own ERP).
Skills practised:
- Technology
- Software Engineering
- Organization skills (workflows)
- Automation

### Business Management
3 years managing the family business in Transportation. In charge of all departments (Sales, Finance, Technology, etc).
Skills practised:
- IT infrastructure
- Leadership
- Management
- Organisation

# OTHER INTERESTS
Passionate about motorsports, sailing, and practitioner of martial arts. Very handy with any IT or DIY related fields.
